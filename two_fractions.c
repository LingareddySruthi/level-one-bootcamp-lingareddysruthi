
#include<stdio>.h 
struct fraction
{
   int n,d;
};
struct fraction_pair
{
          struct fraction x;
          struct fraction y;
};

struct final_result
{
    int numerator,denominator;
};

struct fraction_pair get_input()
{
   struct fraction_pair fract;
   printf("\nEnter the numerator for 1st number: ");
   scanf("%d",&fract.x.n);
   printf("\nEnter the denominator for 1st number: ");
   scanf("%d",&fract.x.d);
   printf("\nEnter the numerator for 2nd number: ");
   scanf("%d",&fract.y.n);
   printf("\nEnter the denominator for 2nd number: ");
   scanf("%d",&fract.y.d);
   return fract;
}

int get_gcd(int m, int n)
{
   int c,gcd;
   for(c=1; c<=m && c<=n; ++c)
{
  if(m%c==0 && n%c==0)
  gcd = c;
 }
 return gcd;
}

struct final_result calculate(struct fraction_pair calc)
{
    struct final_result fract;
    int m,n,gcd_no;
    m=(calc.x.n*calc.y.d)+(calc.x.d*calc.y.n);
    n=calc.x.d*calc.y.d;
    gcd_no=get_gcd(m,n);
    fract.numerator=m/gcd_no;
    fract.denominator=n/gcd_no;
    return fract;
}

void display_output(struct final_result final)
{
                 printf("\nThe added fraction is %d/%d", final.numerator,final.denominator);
                 printf("\n");
}


int main()
{
     struct final_result final;  
     struct fraction_pair value;
     value=get_input();
      result=calculate(value);
      display_output(result);


}
   


